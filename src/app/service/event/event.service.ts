import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DOMAIN, URI } from "../../constants/application.constants";
import { Event } from "../../beans/event.bean";

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private http: HttpClient) { }

  getEvents() {
    return this.http.get<Event[]>(DOMAIN+URI.GET_EVENTS).toPromise();
  }

  createEvent(event: Event) {
    return this.http.put(DOMAIN+URI.CREATE_EVENT, event).toPromise();
  }

  editEvent(event: Event) {
    return this.http.post(DOMAIN+URI.CREATE_EVENT, event).toPromise();
  }

  deleteEvent(event: Event) {
    return this.http.delete(DOMAIN+URI.CREATE_EVENT+`/${event.id}`).toPromise();
  }

  participate(event: Event) {
    return this.http.get(DOMAIN+URI.PARTICIPATE(event.id)).toPromise();
  }
}
