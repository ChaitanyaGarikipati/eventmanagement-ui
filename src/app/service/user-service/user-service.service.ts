import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DOMAIN, URI } from "../../constants/application.constants";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  login(userName: string, password: string) {
    return this.http.post(DOMAIN + URI.LOGIN, {
      username: userName,
      password: password
    }, {withCredentials: true}).toPromise();
  }

  signUp(userName: string, email: string, password: string) {
    return this.http.post<any>(DOMAIN + URI.SIGNUP, {
      username: userName,
      email: email,
      password: password
    }).toPromise();
  }


  logout() {
    console.log(DOMAIN + URI.LOGOUT);
    
    return this.http.get(DOMAIN + URI.LOGOUT).toPromise();
  }
}
