export interface Event {
    id?: number;
    name: string;
    description: string;
    duration: number; // in hours
    location: string;
    fees: number;
    tags: string | any;
    maxParticipants: number;
    customFields?: any;
    subscribed?: boolean;
}