import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SUBSCRIPTION_ID } from '../constants/application.constants';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.log(localStorage.getItem(SUBSCRIPTION_ID));
    const subscriptionId = localStorage.getItem(SUBSCRIPTION_ID)
    if (!subscriptionId) {
      this.router.navigateByUrl('/login');
    }
    return true;
  }
  
}
