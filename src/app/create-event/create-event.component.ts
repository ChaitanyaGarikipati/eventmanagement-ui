import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Event } from "../beans/event.bean";
import { EventService } from '../service/event/event.service';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {
  event: Event;
  isSignup = false;

  form: FormGroup;

  async save() {
    if (this.form.valid) {
      this.submitEM.emit(this.form.value);

      this.event = {
        id: this.data?.event?.id,
        name: this.form.get('name').value,
        description: this.form.get('description').value,
        duration: this.form.get('duration').value,
        fees: this.form.get('fees').value,
        location: this.form.get('location').value,
        maxParticipants: this.form.get('maxParticipants').value,
        tags: this.form.get('tags').value
      };      
      this.event.tags = this.event.tags instanceof Object ? this.event.tags : this.event.tags?.split(',');
      let result;
      if (this.data?.action === "edit") {
        result = await this.eventService.editEvent(this.event);
      } else {
        result = await this.eventService.createEvent(this.event);
      }
      
      console.log(result);
      this.dialogRef.close();
    }
  }
  @Input() error: string | null;

  @Output() submitEM = new EventEmitter();
  constructor(private eventService: EventService,
    public dialogRef: MatDialogRef<CreateEventComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {action: string, event: Event}) {
      this.event = this.data?.event;
      console.log(this.data?.event);
      this.form = new FormGroup({
        name: new FormControl(this.event?.name),
        description: new FormControl(this.event?.description),
        duration: new FormControl(this.event?.duration),
        location: new FormControl(this.event?.location),
        fees: new FormControl(this.event?.fees),
        tags: new FormControl(this.event?.tags),
        maxParticipants: new FormControl(this.event?.maxParticipants)
      });
    }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close();
  }

}
