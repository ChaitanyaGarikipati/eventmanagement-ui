import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../service/user-service/user-service.service";
import {Router} from "@angular/router"
import { SUBSCRIPTION_ID, USER_ID } from '../constants/application.constants';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})
export class LoginComponent implements OnInit {
  isSignup = false;
  userId: string;

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', {validators: [Validators.required]}),
    password: new FormControl('', [Validators.required])
  });

  signupForm: FormGroup = new FormGroup({
    username: new FormControl('', {validators: [Validators.required]}),
    password: new FormControl('', [Validators.required]),
    email: new FormControl('', {validators: [Validators.required, Validators.email]}),
    cnfPassword: new FormControl('', {validators: [Validators.required]})
  });

  async login() {
    if (this.loginForm.valid) {
      try {
        const result = await this.userService.login(this.loginForm.get('username').value, this.loginForm.get('password').value);
        console.log(result[SUBSCRIPTION_ID]);
        localStorage.setItem(SUBSCRIPTION_ID, result[SUBSCRIPTION_ID]);
        this.userId = result['id'];
        localStorage.setItem(USER_ID, this.userId);
        this.router.navigate(['/dashboard']);
      } catch (error) {
        this.error = error.error;
      }
    }
  }
  @Input() error: string | null;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  async signup() {
    this.isSignup = true;
    if (this.signupForm.valid) {
      try {
        await this.userService.signUp(this.signupForm.get('username').value, this.signupForm.get('email').value, this.signupForm.get('password').value);
      } catch (error) {
        this.error = error.error;
      }
    }
  }
}
