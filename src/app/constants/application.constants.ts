export const DOMAIN = "http://localhost:8080";
export const URI = {
    LOGIN: "/login",
    SIGNUP: "/signup",
    LOGOUT: "/logout",
    GET_EVENTS: "/event/all",
    GET_EVENT: "/event", // append id path param.
    CREATE_EVENT: "/event",
    PARTICIPATE: (id) => `${URI.CREATE_EVENT}/${id}/participate`
};
export const SUBSCRIPTION_ID = "subscriptionId";
export const USER_ID = "user_id";
