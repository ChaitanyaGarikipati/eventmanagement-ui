import { Component, OnInit, Inject } from '@angular/core';
import { EventService } from "../service/event/event.service";
import { Event } from '../beans/event.bean';
import { MatDialog } from '@angular/material/dialog';
import { CreateEventComponent } from '../create-event/create-event.component';
import { SUBSCRIPTION_ID, USER_ID } from '../constants/application.constants';
import { UserService } from '../service/user-service/user-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [EventService]
})
export class DashboardComponent implements OnInit {
  events: Event[];
  subscriptions: any[];
  userId: string;
  showAddEventForm = false;
  event: Event;

  constructor(private eventService: EventService,
    public dialog: MatDialog,
    private userService: UserService,
    private router: Router) {
    // var sse = new EventSource(`http://localhost:8080/demo/${localStorage.getItem('subscriptionId')}`);
    // sse.onmessage = function (evt) {
    //   console.log(evt);
    // };
    this.userId = localStorage.getItem(USER_ID);
    var sse = new EventSource(`http://localhost:8080/notifications/${localStorage.getItem(SUBSCRIPTION_ID)}`, {withCredentials: true});
    sse.onmessage = function (evt) {
      console.log(evt.data);
    };
  }

  ngOnInit(): void {
    this.init();
  }

  init() {
    this.eventService.getEvents().then(data => {
      this.events = data['events'];
      this.subscriptions = data['subscriptions'];
      this.events = this.events.map(e => {
        if (this.subscriptions.some(s => s.eventId === e.id)) {
          e.subscribed = true;
        }
        return e;
      });
    });
  }

  toggleAddEventForm() {
    this.showAddEventForm = true;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreateEventComponent, {
      width: '75%'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.init();
    });
  }

  showNotifications() {
    
  }

  logout() {
    this.userService.logout();
    this.router.navigateByUrl('/login');
  }


  edit(event) {
    console.log(event);
    const dialogRef = this.dialog.open(CreateEventComponent, {
      width: '75%',
      data: { action: 'edit', event: event }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.init();
    });
  }

  async delete(event) {
    console.log(event);
    const result = await this.eventService.deleteEvent(event);
    console.log(result);
    this.init();
  }

  async participate(event) {
    console.log(event);
    const result = await this.eventService.participate(event);
    console.log(result);
    this.init();
  }

}
